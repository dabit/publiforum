Rails.application.routes.draw do
  get 'users/new'
  root to: 'messages#index'
  scope :forum do
    resource :user, only: [:new, :create]
    resources :messages, only: [:index, :create]
  end
end
