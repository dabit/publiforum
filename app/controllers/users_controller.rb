class UsersController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.valid?
      cookies.encrypted[:user_name] = @user.name
      redirect_to :messages
    else
      render :new
    end
  end

  def user_params
    params.require(:user).permit(:name)
  end
end
