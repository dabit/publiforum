class MessagesController < ApplicationController
  before_action :set_current_user

  def index
    @message = Message.new
    @messages = Message.order(created_at: :desc).first(30)
  end

  def create
    @message = Message.new(message_params)
    @message.from = @current_user
    if @message.save
      redirect_to :messages
    else
      render action: :index
    end
  end

  private

  def set_current_user
    @current_user = cookies.encrypted[:user_name]
    redirect_to new_user_path unless @current_user
  end

  def message_params
    params.require(:message).permit(:from, :body)
  end
end
