class ForumsChannel < ApplicationCable::Channel
  def subscribed
    stream_from "forum_updates"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
