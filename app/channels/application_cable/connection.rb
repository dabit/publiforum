module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user

    def connect
      user_name = cookies.encrypted[:user_name]
      return reject_unauthorized_connection if user_name.nil?

      self.current_user = user_name
    end
  end
end
