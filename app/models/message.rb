class Message < ApplicationRecord
  validates :from, presence: true
end
